DIR="/var/www"
for dir in $DIR/*/; do
	echo "Checking "$dir" ..."
	# Get owner information
	if [ -f "$dir/web/index.php" ]; then
	user=`ls -l $dir/web/index.php | awk '{print $3}'`
	group=`ls -l $dir/web/index.php | awk '{print $4}'`
	fi
	if [ -d "$dir/web/wp-content/plugins/w3-total-cache" ]; then
		echo "Found W3 Total Cache - Upgrading..."
		rm -rf $dir/web/wp-content/plugins/w3-total-cache
		cp -R w3-total-cache $dir/web/wp-content/plugins/
		chown -R $user:$group $dir/web/wp-content/plugins/w3-total-cache
		echo "Done."
	fi
	if [ -d "$dir/web/wp-content/plugins/wp-super-cache" ]; then
		echo "Found WP Super Cache - Upgrading..."
		rm -rf $dir/web/wp-content/plugins/wp-super-cache
		cp -R wp-super-cache $dir/web/wp-content/plugins/
		chown -R $user:$group $dir/web/wp-content/plugins/wp-super-cache
		echo "Done."
	fi
done
