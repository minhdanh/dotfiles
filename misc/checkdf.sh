#!/bin/bash
 
#------------------------------------------------#
# Search web files for potential malicious code. #
#------------------------------------------------#
 
SEARCH_DIR=$1
PATTERNS="passthru|shell_exec|system\(|phpinfo|base64_decode|popen|exec\(|proc_open|pcntl_exec|python_eval|fopen|fclose|readfile"
 
grep -RP --colour=auto --include=*.{php,txt} "($PATTERNS)" $SEARCH_DIR
 
exit 0
