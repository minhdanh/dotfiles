#!/bin/bash
WP_PASS=`grep PASS $1web/wp-config.php | cut -d\' -f4`
WP_USER=`grep USER $1web/wp-config.php | cut -d\' -f4`
WP_DBNAME=`grep DB_NAME $1web/wp-config.php | cut -d\' -f4`
# If not wordpress, exit
if [ "$WP_DBNAME" == "" ]; then
	echo "Not a wordpress install. Now exit."
	exit 0
fi
echo "Hardening $1..."
# Check if this is a multisite
IS_MULTISITE=`grep "MULTISITE" $1web/wp-config.php | awk '{print $2}' | cut -d")" -f1`

if [ -n "$IS_MULTISITE" ]; then
	if [ "$IS_MULTISITE" == "true" ]; then
		echo "$1 is multisite"
	fi
fi
echo "1. Changing permissions..."
find $1web/ -type d -exec chmod 755 {} \;
find $1web/ -type f -exec chmod 644 {} \;
echo "2. Password protecting the wp-admin directory..."
echo "tgmcorp:{SHA}Yu3eDWOgR05lyD6enHPYWn968TE=" > $1web/wp-admin/.htpasswd
echo "AuthUserFile /var/www/$1web/wp-admin/.htpasswd
AuthType basic
AuthName \"Restricted Resource\"
require valid-user
<Files admin-ajax.php>
    Order allow,deny
    Allow from all
    Satisfy any 
</Files>
<FilesMatch \.(css|png|gif|jpg|jpeg|js)>
    Order allow,deny
    Allow from all
    Satisfy any
</FilesMatch>" > $1web/wp-admin/.htaccess

echo "3. Securing wp-includes (ommitted for multisite)"
if [ "$IS_MULTISITE" != "true" ]; then
	if [ "`grep '\# END WordPress' $1web/.htaccess`" == "# END WordPress" ]; then
		echo "Writing to .htaccess..."
		sed -i '/# END WordPress/a \
<IfModule mod_rewrite.c> \
# Block the include-only files. \
RewriteEngine On \
RewriteBase / \
RewriteRule ^wp-admin/includes/ - [F,L] \
RewriteRule !^wp-includes/ - [S=3] \
RewriteRule ^wp-includes/[^/]+\.php$ - [F,L] \
RewriteRule ^wp-includes/js/tinymce/langs/.+\.php - [F,L] \
RewriteRule ^wp-includes/theme-compat/ - [F,L] \
</IfModule>' $1web/.htaccess

	fi
fi

echo "4. Disabling file editing..."
WPLANG=`grep "define ('WPLANG" $1web/wp-config.php | cut -d\( -f2 | cut -d, -f1 | cut -d\' -f2`
if [ -n $WPLANG ]; then
	sed -i "/define ('WPLANG/a \
define ('DISALLOW_FILE_EDIT', true);" $1web/wp-config.php
else
	echo "No signs found, skipping..."
fi

echo "5. Securing wp-content..."
PHPFILES=`find $1web/wp-content/uploads/ -name "*.php" | wc -l`
if [ "$PHPFILES" -gt "0" ]; then
	echo "Found php file(s) in upload directory, skipping..."
else
	echo "<FilesMatch .php>
Order Deny,Allow
Deny from All
</FilesMatch>" > $1web/wp-content/uploads/.htaccess
fi

echo "6. Checking for malicious posts..."
iframe=`mysql -u $WP_USER -p$WP_PASS $WP_DBNAME -s -e "SELECT COUNT(*) FROM wp_posts WHERE post_content LIKE '%<iframe%';"`
if [ "$iframe" -ne "0" ]; then
	echo "Possible iframe attacks!"
fi
noscript=`mysql -u $WP_USER -p$WP_PASS $WP_DBNAME -s -e "SELECT COUNT(*) FROM wp_posts WHERE post_content LIKE '%<noscript%';"`
if [ "$noscript" -ne "0" ]; then
	echo "Possible noscript attacks!"
fi
display=`mysql -u $WP_USER -p$WP_PASS $WP_DBNAME -s -e "SELECT COUNT(*) FROM wp_posts WHERE post_content LIKE '%display:%';"`
if [ "$display" -ne "0" ]; then
	echo "Possible css attacks!"
fi

echo "7. Removing wordpress meta tag generator..."
cd /var/www/$1web/wp-content/themes/
for file in *

do
        if [ -d "$file" ]; then
		if [ -f "/var/www/$1web/wp-content/themes/$file/functions.php" ]; then
			echo "Found functions.php, applying patch..."
			IFFIXED=`grep "remove_action" $file/functions.php | grep "wp_head" | grep "wp_generator" | cut -d_ -f1`
			if [ "$IFFIXED" != "remove" ]; then
				echo "
<?php remove_action('wp_head', 'wp_generator'); ?>" >> $file/functions.php	
			fi
		fi
        fi
done
exit 0
