#!/bin/bash
WP_PASS=`grep PASS $1web/wp-config.php | cut -d\' -f4`
WP_USER=`grep USER $1web/wp-config.php | cut -d\' -f4`
WP_DBNAME=`grep DB_NAME $1web/wp-config.php | cut -d\' -f4`
echo "Database: " $WP_DBNAME
echo "User: " $WP_USER
echo "Pass: " $WP_PASS
read -p "Are you sure? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo  
    echo "Beginning to backup" $1 "..."
    echo "Archiving " $1
    cd $1
    tar cvf web.tar web/
    echo "Dumping database..."
    mysqldump -u $WP_USER -p$WP_PASS $WP_DBNAME > sql.sql
#    echo "Uploading files to remote servers..."
#    scp -i ~/.ssh/id_dsa web.tar root@tgm.vn:/var/www/$1
#    scp -i ~/.ssh/id_dsa sql.sql root@tgm.vn:/var/www/$1
#    echo "Done."
#    echo "Extracting files and restoring database..."
#    WEBDIR=/var/www/$1
#    ssh -i ~/.ssh/id_dsa root@tgm.vn "cd $WEBDIR;tar xvf web.tar;mysql -u $2 -p$3 $4 < sql.sql;rm web.tar;rm sql.sql;cd web;rm -f index.html;rm -f robots.txt";
#    echo "Removing files..."
#    rm -rf web.tar sql.sql
    echo "Done!"
fi
echo  
exit 0
